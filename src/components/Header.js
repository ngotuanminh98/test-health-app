import React from 'react'
import { LinkContainer } from 'react-router-bootstrap'
import { Navbar, Nav, Container, Dropdown } from 'react-bootstrap'

const Header = () => {
  return (
    <header>
      <Navbar variant='dark' expand='lg' collapseOnSelect style={{backgroundColor: '#414141'}}>
        <Container>
          <LinkContainer to='/'>
            <Navbar.Brand>
              <img src={'logo.svg'} alt='' />
            </Navbar.Brand>
          </LinkContainer>
          <Navbar.Toggle aria-controls='basic-navbar-nav' />
          <Navbar.Collapse id='basic-navbar-nav'>
            <Nav className='ml-auto'>

              <LinkContainer to='/'>
                <Nav.Link className='link-header'>
                  <img src='images/document.svg' alt='' /> 自分の記録
                </Nav.Link>
              </LinkContainer>

              <LinkContainer to='/'>
                <Nav.Link className='link-header'>
                  <img src='images/challenge.svg' alt='' /> チャレンジ
                </Nav.Link>
              </LinkContainer>

              <LinkContainer to='/'>
                <Nav.Link className='link-header'>
                  <img src='images/info.svg' alt='' /> お知らせ
                </Nav.Link>
              </LinkContainer>

              <Dropdown>
                <Dropdown.Toggle id="dropdown-basic" style={{backgroundColor: '#414141'}}>
                  <img src='images/icon_menu.svg' alt='' />
                </Dropdown.Toggle>

                <Dropdown.Menu className='dropdown-menu'>
                  <LinkContainer to='/'>
                    <Dropdown.Item>自分の記録</Dropdown.Item>
                  </LinkContainer>
                  <LinkContainer to='/'>
                    <Dropdown.Item>体重グラフ</Dropdown.Item>
                  </LinkContainer>
                  <LinkContainer to='/'>
                    <Dropdown.Item>目標</Dropdown.Item>
                  </LinkContainer>
                  <LinkContainer to='/'>
                    <Dropdown.Item>選択中のコース</Dropdown.Item>
                  </LinkContainer>
                  <LinkContainer to='/'>
                    <Dropdown.Item>コラム一覧</Dropdown.Item>
                  </LinkContainer>
                  <LinkContainer to='/'>
                    <Dropdown.Item>設定</Dropdown.Item>
                  </LinkContainer>
                </Dropdown.Menu>
              </Dropdown>
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
    </header>
  )
}

export default Header

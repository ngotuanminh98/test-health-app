import React from 'react'
import { Container, Row } from 'react-bootstrap'
import { LinkContainer } from 'react-router-bootstrap'

const Footer = () => {
  return (
    <footer className='footer'>
      <Container>
        <Row className='flex-row'>
          <LinkContainer to='/'>
            <p className='footer-item'>会員登録</p>
          </LinkContainer>
          <LinkContainer to='/'>
            <p className='footer-item'>運営会社</p>
          </LinkContainer>
          <LinkContainer to='/'>
            <p className='footer-item'>利用規約</p>
          </LinkContainer>
          <LinkContainer to='/'>
            <p className='footer-item'>個人情報の取扱について</p>
          </LinkContainer>
          <LinkContainer to='/'>
            <p className='footer-item'>特定商取引法に基づく表記</p>
          </LinkContainer>
          <LinkContainer to='/'>
            <p className='footer-item'>お問い合わせ</p>
          </LinkContainer>
        </Row>
      </Container>
    </footer>
  )
}

export default Footer

import React from 'react'
import { Row } from 'react-bootstrap'
import Meta from '../components/Meta'
import {
  LineChart,
  Line,
  XAxis,
  CartesianGrid,
} from "recharts";
import useWindowDimensions from '../hook/useWindowDimensions';

const data = [
  {
    name: "6月",
    uv: 10000,
    pv: 10000,
    amt: 2400
  },
  {
    name: "7月",
    uv: 8500,
    pv: 9000,
    amt: 2210
  },
  {
    name: "8月",
    uv: 7000,
    pv: 6500,
    amt: 2290
  },
  {
    name: "9月",
    uv: 6800,
    pv: 7300,
    amt: 2000
  },
  {
    name: "10月",
    uv: 6000,
    pv: 6800,
    amt: 2181
  },
  {
    name: "11月",
    uv: 6200,
    pv: 6500,
    amt: 2500
  },
  {
    name: "12月",
    uv: 5800,
    pv: 7300,
    amt: 2100
  },
  {
    name: "1月",
    uv: 5500,
    pv: 6300,
    amt: 2100
  },
  {
    name: "2月",
    uv: 5000,
    pv: 5800,
    amt: 2100
  },
  {
    name: "3月",
    uv: 4000,
    pv: 5300,
    amt: 2100
  },
  {
    name: "4月",
    uv: 3500,
    pv: 5000,
    amt: 2100
  },
  {
    name: "5月",
    uv: 3300,
    pv: 5300,
    amt: 2100
  }
];

const Chart = () => {
  const { width } = useWindowDimensions();
  const widthChart = width*2/3;
  return(
    <LineChart
      className='line-chart'
      width={widthChart}
      height={300}
      data={data}
      margin={{
        top: 18,
        right: 36,
        left: 36,
        bottom: 18
      }}
    >
      <CartesianGrid strokeDasharray="1 1" />
      <XAxis dataKey="name" />
      <Line type="linear" dataKey="pv" stroke="#FFCC21" activeDot={{ r: 8 }} />
      <Line type="linear" dataKey="uv" stroke="#8FE9D0" />
    </LineChart>
  )
}

const HomeScreen = ({ match }) => {

  return (
    <>
      <Meta />
      <Row className='w-100 m-0'>
        {/* <Col className='p-0' xs="4" md="4"> */}
        <div className='wrap-banner'>
          <img className='image-banner' src='images/banner1.png' alt='' />
        </div>
        {/* </Col> */}
        <div className='wrap-chat'>
          <Chart />
        </div>
      </Row>
    </>
  )
}

export default HomeScreen
